# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
PYTHON_DEPEND="*"

inherit eutils python distutils

DESCRIPTION="XML library."
HOMEPAGE="http://home.avvanta.com/~steveha/xe.html"
SRC_URI="http://www.blarg.net/%7Esteveha/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
